terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.17.1"
    }
  }
}

provider "aws" {
  region  = var.region
}

locals {
  count = "1"
  ami_id = "ami-0e36349d43689d074"
  xuel_tag = {
    application  = "xuhao_app"
    environment  = "dev"
    purpose      = "company use"
    contact      = "xuh@newmindtech.cn"
    creator      = "xuhao"
    owner        = "xuhao"
    company      = "newmind"
  }
}


data "aws_ec2_instance_type_offering" "instance_type" {
  filter {
    name   = "instance-type"
    values = ["t3.medium", "c5.xlarge"]
  }

  preferred_instance_types= ["t3.medium", "c5.xlarge"]
}

data "template_file" "cloud_init" {
  template = file("init.tpl")
}


resource "aws_instance" "server" {
  #ami                    = var.IsCentos ? data.aws_ami.centos.id : data.aws_ami.linux2.id
  ami                    = local.ami_id
  count                  = local.count
  instance_type          = data.aws_ec2_instance_type_offering.instance_type.id
  #subnet_id              = aws_subnet.prod-subnet-public-1.id
  subnet_id              = "subnet-01166dc792f44fe54"
  #vpc_security_group_ids = ["${aws_security_group.ec2_allow_rule.id}"]
  vpc_security_group_ids = ["sg-0d58009936da81588"]
  user_data              = data.template_file.cloud_init.rendered
  key_name               = "newmind"
  tags                   = local.xuel_tag

  root_block_device {
    volume_size = var.root_volume_size # 20 GB 
  }

  ebs_block_device {
      device_name = "/dev/sdb"
      delete_on_termination = true
      encrypted             = true
      volume_size           = 10
      volume_type           = "gp2"
  }
}


output "instance_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.server.*.public_ip
}

// Sends your public key to the instance
#resource "aws_key_pair" "mykey-pair" {
#  key_name   = "mykey-pair"
#  public_key = file(var.PUBLIC_KEY_PATH)
#}

