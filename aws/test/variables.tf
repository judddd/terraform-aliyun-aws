variable "region" {
  type    = string
  default = "cn-northwest-1"
}

variable "az" {
  type    = string
  default = "cn-northwest-1a"
}
variable "IsCentos" {
  type    = bool
  default = false

}


variable "public_subnet_cidrs" {
  type        = list(string)
  description = "Public Subnet CIDR values"
  default     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

variable "private_subnet_cidrs" {
  type        = list(string)
  description = "Private Subnet CIDR values"
  default     = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
}

variable "VPC_cidr" {
  type    = string
  default = "10.0.0.0/16"
}
variable "subnet1_cidr" {
  type    = string
  default = "10.0.1.0/24"
}
variable "subnet2_cidr" {
  type    = string
  default = "10.0.2.0/24"
}
variable "subnet3_cidr" {
  type    = string
  default = "10.0.3.0/24"
}
# variable "instance_type" {}
variable "instance_class" {
  type    = string
  default = "db.t3.micro"
}
variable "PUBLIC_KEY_PATH" {
  type    = string
  default = "/root/.ssh/id_rsa.pub"
}
variable "PRIV_KEY_PATH" {
  type    = string
  default = "/root/.ssh/id_rsa"
}
variable "root_volume_size" {
  type    = number
  default = 20
}
