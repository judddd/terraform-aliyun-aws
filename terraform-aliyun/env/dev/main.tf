provider "alicloud" {
  access_key = var.alicloud_access_key
  secret_key = var.alicloud_secret_key
  region     = var.region
}

//  local变量
locals {
  region         = "cn-hangzhou-h"
  vpc_cidr_block = "172.80.0.0/12"
  vsw_cidr_block = "172.80.0.0/21"
  #private_ips    = ["172.80.0.1"]
  private_ips    = ["172.80.0.1","172.80.0.2","172.80.0.3","172.80.0.4","172.80.0.5","172.80.0.6","172.80.0.7","172.80.0.8","172.80.0.9","172.80.0.10","172.80.0.11","172.80.0.12","172.80.0.13","172.80.0.14","172.80.0.15","172.80.0.16","172.80.0.17","172.80.0.18"]
  number_of_instances = "2"
  vpc_name       = "myvpc"
  name = "test"

  user_data = <<-EOF
  #!/bin/bash
  # until [[ -f /var/lib/cloud/instance/boot-finished ]] ;do
  #    sleep 1
  # done
  echo "
  172.80.0.1 rancher001
  172.80.0.2 rancher002
  172.80.0.3 rancher003
  " | tee -a /etc/hosts

  echo "
  #!bin/bash
  sed -i 's/enforcing/disabled/' /etc/selinux/config
  setenforce 0

  systemctl disable firewalld
  systemctl stop firewalld
  systemctl status firewalld
  systemctl disable NetworkManager
  systemctl stop NetworkManager
  systemctl status NetworkManager

  curl https://kubernetes.pek3b.qingstor.com/tools/kubekey/docker-install.sh | sh && systemctl enable docker && systemctl restart docker&& echo ewogICAgIm9vbS1zY29yZS1hZGp1c3QiOiAtMTAwMCwKICAgICJsb2ctZHJpdmVyIjogImpzb24tZmlsZSIsCiAgICAibG9nLW9wdHMiOiB7CiAgICAgICAibWF4LXNpemUiOiAiMTAwbSIsCiAgICAgICAibWF4LWZpbGUiOiAiMyIKICAgIH0sCiAgICAibWF4LWNvbmN1cnJlbnQtZG93bmxvYWRzIjogMTAsCiAgICAiaW5zZWN1cmUtcmVnaXN0cmllcyI6IFsiMC4wLjAuMC8wIl0sCiAgICAibWF4LWNvbmN1cnJlbnQtdXBsb2FkcyI6IDEwLAogICAgInJlZ2lzdHJ5LW1pcnJvcnMiOiBbImh0dHBzOi8vZG9ja2VyaHViLmF6azhzLmNuIl0sCiAgICAic3RvcmFnZS1kcml2ZXIiOiAib3ZlcmxheTIiLAogICAgImV4ZWMtb3B0cyI6IFsibmF0aXZlLmNncm91cGRyaXZlcj1zeXN0ZW1kIl0sCiAgICAic3RvcmFnZS1vcHRzIjogWwogICAgIm92ZXJsYXkyLm92ZXJyaWRlX2tlcm5lbF9jaGVjaz10cnVlIgogICAgXQp9Cg== | base64 -d > /etc/docker/daemon.json && systemctl reload docker && systemctl restart docker " >> /root/init.sh
  EOF
}

// data resource配置
data "alicloud_instance_types" "spot_t5" {
  availability_zone = local.region
  instance_type_family = "ecs.t5"
  spot_strategy     = "SpotAsPriceGo"
  cpu_core_count    = 2
  memory_size       = 4
}

// 突发性实例
data "alicloud_instance_types" "t6" {
  availability_zone    = local.region
  instance_type_family = "ecs.t6"
  cpu_core_count       = 2
  memory_size          = 4
}

data "alicloud_images" "centos" {
  owners       = "system"
  name_regex   = "^centos_7_9"
  architecture = "x86_64"
  status       = "Available"
  os_type      = "linux"
}

data "alicloud_images" "ubuntu" {
  most_recent = true
  name_regex  = "^ubuntu_18.*64"
}


// module配置
module "myssecgroup" {
  source = "../../modules/secgroup"
  vpc_id = module.myvpc.vpc_id
}

module "myvpc" {
  source         = "../../modules/vpc"
  vpc_cidr_block = local.vpc_cidr_block
  vsw_cidr_block = local.vsw_cidr_block
  vpc_name       = local.vpc_name
  zone_id        = local.region
}

module "myecs" {
  source      = "../../modules/ecs"
  region      = local.region
  vsw_id      = module.myvpc.vsw_id
  secgroup_id = module.myssecgroup.secgroup_id
  image_id                    = data.alicloud_images.centos.ids.0
  instance_type               = data.alicloud_instance_types.t6.ids.0
  spot_strategy               = "SpotWithPriceLimit"
  disk_name                   = "my_module_disk"
  system_disk_size            = 40
  internet_max_bandwidth_out  = 100
  associate_public_ip_address = true
  instance_name               = "my_module_instances_"
  host_name                   = "sample"
  internet_charge_type        = "PayByTraffic"
  number_of_instances         = local.number_of_instances
  password                    = "newmind@123"
  #user_data = local.user_data
  private_ips                 = local.private_ips

  #instance_type              = "ecs.t5-c1m2.2xlarge"
  #system_disk_category       = "cloud_ssd"
  #private_ips                = ["172.16.0.10"]
  #image_ids                  = ["centos_7_8_x64_20G_alibase_20200519.vhd"]
  # data_disks                    = [
  # {
  #   name                  = "/dev/xvdb"
  #   size                  = 200
  #   delete_with_instance  = true
  # },
  # {
  #   name                  = "/dev/xvdc"
  #   size                  = 100
  #   delete_with_instance  = true
  # }
  #]
}

output "my_public_ip" {
  value = module.myecs.this_public_ip
}
output "my_private_ip" {
  value = module.myecs.private_ips
}
